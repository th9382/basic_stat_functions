"""
Mystats class with methods to calculate mean, median, mode
"""

import sys

class Mystats(object):

    def mean(self, arr):
        """calculate mean"""
        if arr:
            total = self.sum(arr)

            n = float(len(arr))
            return total / n

    def median(self, arr):
        """calculate median"""
        if arr:
            # sort array ascending
            arr = sorted(arr)
            length = len(arr)
            mid_point = length//2

            if length % 2 != 0:  # if array has odd number of elements
                return arr[mid_point]
            else:
                return (arr[mid_point - 1] + arr[mid_point]) / 2

    def mode(self, arr):
        """calcualte mode. if no repeating numbers, return None"""
        if arr:
            freq_dict = self.freq(arr)

            # determine most frequent number
            highest = 0
            for v in freq_dict.values():
                if v >= highest:
                    highest = v

            # if no repeating numbers, return None
            if highest == 1:
                return None 

            # create array of number(s) with most frequent occurances
            mode = []
            for k, v in freq_dict.items():
                if v == highest:
                    mode.append(k)

            return mode

    def sum(self, arr):
        """calculates sum of numbers in array"""
        if arr:
            s = 0
            for num in arr:
                s += num
            return s

    def freq(self, arr):
        """create dictionary showing frequency of each number in array"""
        if arr:

            freq_dict = {}

            for val in arr:
                if val in freq_dict:
                    freq_dict[val] += 1
                else:
                    freq_dict[val] = 1
            return freq_dict


class Calculator(object):
    def __init__(self):
        self.mystats = Mystats()

    def run(self):
        command = ''
        while command != 'quit':
            num_array = raw_input("Type in a series of numbers (comma separated) and get the mean, median, and mode:\n>> ")
            num_array = num_array.split(",")

            # convert array items to float type
            try:
                for i in range(len(num_array)):
                    num_array[i] = float(num_array[i])
            except:
                print "Error with your input. You can only enter numbers. Try again"
                continue

            print "\n--- Summary ---\n"
            print "array: ", num_array
            print "mean: ", self.mystats.mean(num_array)
            print "median: ", self.mystats.median(num_array)
            print "mode: ", self.mystats.mode(num_array)

            command = str(raw_input("\nType 'quit' to quit program. Otherwise type anything else to continue:\n>> "))
        print "Quit!"
        sys.exit(0)


if __name__ == '__main__':
    calc = Calculator().run()




